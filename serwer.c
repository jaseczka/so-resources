/********************************
 * Autor: Magdalena Teperowska
 * Plik: serwer.c
 * Data: 23.12.2013
 *
 ********************************/

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>
#include "err.h"
#include "msgkey.h"

#define MAX_K 99 
#define MAX_N 9999

#ifdef DEBUG
int debug = 1;
#else
int debug = 0;
#endif

/* numer kolejki na którą są wysyłane wiadomości dla klientów */
int msg_to_client;
/* numer kolejki na którą są wysyłane wiadomości dla serwera */
int msg_to_server;
int res[MAX_K]; /* tablica zasobów */
int demand[MAX_K]; /* ile zasobów chcą zużyć sparowani klienci */
int pid[MAX_K]; /* do wymieniania pidu między wątkami */
pthread_t thread[MAX_K]; /* do wymieniania deskryptorów między wątkami */
int working; /* licznik pracujących */
int unpaired[MAX_K]; /* licznik niesparowanych */
int pairing[MAX_K]; /* czy jest klient chętny do sparowania */
pthread_mutex_t protect;
/* dla par czekających na zwolnienie zasobów */
pthread_cond_t res_cond[MAX_K];
/* dla czekających na sparowanie */
pthread_cond_t unpaired_cond[MAX_K];
pthread_attr_t attr;

/* struct do przekazywania argumentów dla nowego wątku */
typedef struct {
    int pid;
    int k;
    int n;
} data_t;

/* funkcja uruchamiana przy SIGINT */
void exit_server(int sig) 
{
    int i;
    if (msgctl(msg_to_client, IPC_RMID, 0) == -1)
        syserr("msgctl RMID");
    if (msgctl(msg_to_server, IPC_RMID, 0) == -1)
        syserr("msgctl RMID");
    if (pthread_mutex_destroy(&protect) != 0)
        syserr("mutex destroy");
    if (pthread_attr_destroy(&attr) != 0)
        syserr("attr destroy");
    for (i = 0; i < MAX_K; ++i) {
        if (pthread_cond_destroy(&res_cond[i]) != 0)
            syserr("cond destroy");
        if (pthread_cond_destroy(&unpaired_cond[i]) != 0)
            syserr("cond destroy");
    }
    exit(EXIT_SUCCESS);
}

/* do debugowania, drukuje stan zasobów */
void res_print()
{
    int i;
    fprintf(stderr, "Zasoby: \n");
    for (i = 0; i < MAX_K; ++i)
        fprintf(stderr, "%d ", res[i]);
    fprintf(stderr, "\n");
}

/* funkcja monitora przydzielająca zasób parze.
 * Schemat działania: wątki czekają na sparowanie na conditionie
 * unpaired_cond[k]. W danej chwili, dla danego zasobu jest co najwyżej
 * jedna para czekająca na zasób. Z wątków czekających na sparowanie
 * dwa dobierają się w parę i jeśli nie ma wystarczającej ilości zasobu,
 * czekają na conditionie res_cond[k]. Gdy następuje zwolnienie zasobu
 * wysyłany jest sygnał do res_cond[k]. Gdy para z res_cond[k] ma 
 * rozpocząć konsumpcję zasobów, wysyła sygnał do unpaired_res[k],
 * aby następne dwa wątki (jeśli są) sparowały się.
 */
void res_allocate(int pid_m, int k, int n, int* pid_p, 
pthread_t* thread_p, int* res_sum)
{
    pthread_t th_self;

    if (pthread_mutex_lock(&protect) != 0)
        syserr("lock");

    --k;
    th_self = pthread_self();
    
    /* jeśli jakaś para oczekuje na conditionie res_cond[k]
     * lub jeśli nie ma z kim połączyć się w parę, wątek czeka */
    while (pairing[k] > 1 || (pairing[k] == 0 && unpaired[k] == 0)) {
        if (debug) fprintf(stderr, "[T: %x] Czekam na parę\n",
                        (unsigned int) th_self);
        ++unpaired[k];
        if (pthread_cond_wait(&unpaired_cond[k], &protect) != 0)
            syserr("cond wait");
        --unpaired[k];
    }

    if (pairing[k] == 0 ) { 
        /* jestem pierwszy z pary, mogę kogoś wziąć do pary */
        if (debug) fprintf(stderr, "[T: %x] Inicjalizuję parowanie\n",
                        (unsigned int) th_self);
        ++pairing[k];
        /* używam zmiennych globalnych do wymiany informacji
         * między wątkami. W danej chwili dla danego zasobu tylko
         * jedna para przeprowadza taki proces */
        thread[k] = th_self;
        pid[k] = pid_m;
        demand[k] = n;
        if (pthread_cond_signal(&unpaired_cond[k]) != 0)
            syserr("cond signal");
        while (pid[k] == pid_m)
            if (pthread_cond_wait(&res_cond[k], &protect) != 0)
                syserr("cond wait");
        *pid_p = pid[k];
        *thread_p = thread[k];
        *res_sum = demand[k] + n;
        if (debug) fprintf(stderr, "[T: %x] Namiary drugiego: pid: %d, "
            "t: %x, suma zas: %d\n", (unsigned int) th_self, *pid_p,
            (unsigned int) *thread_p, *res_sum);
        /* Jeśli nie starczy zasobu dla pary, wątek czeka.
         * Chyba, że wątek został wybudzony jako drugi (demand[k] == 0),
         * wtedy wychodzi z pętli */ 
        while (*res_sum > res[k] && demand[k] > 0)
            if (pthread_cond_wait(&res_cond[k], &protect) != 0)
                syserr("cond wait");
    } else if (pairing[k] == 1) { 
        /* jestem drugi z pary */
        if (debug) fprintf(stderr, "[T: %x] Jestem drugi z pary\n",
                        (unsigned int) th_self);
        ++pairing[k];
        *pid_p = pid[k];
        *thread_p = thread[k];
        *res_sum = demand[k] + n;
        thread[k] = th_self;
        pid[k] = pid_m;
        demand[k] = n;
        if (debug) fprintf(stderr, "[T: %x] Namiary na pierwszego: "
            "pid: %d, t: %x, suma zas: %d\n", (unsigned int) th_self,
            *pid_p, (unsigned int) *thread_p, *res_sum);
        if (pthread_cond_signal(&res_cond[k]) != 0)
            syserr("cond signal");
        if (pthread_cond_wait(&res_cond[k], &protect) != 0)
            syserr("cond wait");

    } else {
        fprintf(stderr, "ERROR");
    }

    /* jestem sparowany, będę zjadać zasoby */
    if (debug) fprintf(stderr, "[T: %x] Parowanie dobiegło końca\n",
                        (unsigned int) th_self);

    if (demand[k] > 0) { 
        if (debug) fprintf(stderr, "[T: %x] Pierwszy z randki\n",
                        (unsigned int) th_self);
        res[k] -= *res_sum;
        demand[k] = 0;
        fprintf(stdout, "Wątek %x przydziela %d zasobów %d "
                        "klientom %d %d, pozostało %d zasobów\n",
                        (unsigned int) th_self, *res_sum, k+1,
                        pid_m, *pid_p, res[k]);
        fflush(stdout);
        if (pthread_cond_signal(&res_cond[k]) != 0)
            syserr("cond signal");   
    } else {
        if (debug) fprintf(stderr, "[T: %x] Drugi z randki\n",
                        (unsigned int) th_self);
        pairing[k] = 0;
        if (unpaired[k] > 1)
            if (pthread_cond_signal(&unpaired_cond[k]) != 0)
                syserr("cond signal");   
    }

    if (debug) res_print();

    ++working;

    if (pthread_mutex_unlock(&protect) != 0)
        syserr("unlock");

}

/* funkcja monitora zwalniająca zasób dla pary */
void res_free(int k, int n)
{
    pthread_t th_self;
    if (pthread_mutex_lock(&protect) != 0)
        syserr("lock");
    
    --k;
    th_self = pthread_self();
    
    --working;

    if (debug) fprintf(stderr, "[T: %x] Zwalniam %d zasobu %d\n", 
                    (unsigned int) th_self, n, k+1);

    res[k] += n;

    if (pthread_cond_signal(&res_cond[k]) != 0)
        syserr("cond signal");

    if (debug) res_print(res);

    if (pthread_mutex_unlock(&protect) != 0)
        syserr("unlock");
}

/* funkcja uruchamiana przez nowy wątek, dla każdego klienta */
void *client(void* data)
{
    msgbuf_t msgbuf;
    data_t args = * (data_t *) data;
    int pid_p, res_sum;
    pthread_t thread_p, th_self;

    th_self = pthread_self();

    if (debug) fprintf(stderr, "[T: %x] Wątek otrzymał dane: %d %d %d\n",
                    (unsigned int) th_self, args.pid, args.k, args.n);

    /* przydzielenie zasobów */
    res_allocate(args.pid, args.k, args.n, &pid_p, &thread_p, &res_sum);

    msgbuf.msg_type = args.pid;
    sprintf(msgbuf.msg_data, "%d", pid_p);
    /* powiadomienie klienta */
    if (msgsnd(msg_to_client, &msgbuf, MSG_SIZE, 0) != 0)
        syserr("msgsnd");
    /* odbiór informacji o zakończeniu pracy klienta */
    if (msgrcv(msg_to_server, &msgbuf, MSG_SIZE, args.pid, 0) <= 0)
        syserr("msgrcv");

    /* Aby nie było zakleszczenia tylko jeden wątek czeka
     * na zakończenie drugiego. Czekający odłącza się. */
    if (args.pid < pid_p) {
        if (debug) fprintf(stderr, "[T: %x] Detaszuję się i czekam "
            "na zakończenie drugiego: %x\n", (unsigned int) th_self, 
            (unsigned int) thread_p);
        if (pthread_detach(th_self) != 0)
            syserr("pthread_detach");
        if (debug) fprintf(stderr, "Błąd na joinie!?\n");
        if (pthread_join(thread_p, 0) != 0)
            syserr("pthread_join");
        /* Obaj klienci przestali pracować, można zwolnić zasoby */
        if (debug) fprintf(stderr, "Następuje zwolnienie\n");
        res_free(args.k, res_sum);
    } else {
        if (debug) fprintf(stderr, "[T: %x] Będę dołączany\n", 
                    (unsigned int) th_self);
    }

    free(data);
    
    pthread_exit(0);
}

int main(int argc, char* argv[])
{
    msgbuf_t msgbuf;
    pthread_t th;
    data_t* args;
    int N, K, n, k, pid;
    int i;

    if (signal(SIGINT,  exit_server) == SIG_ERR)
        syserr("signal");

    if ((msg_to_client = msgget(MKEY_CLIENT, 
        0666 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr("msgget");

    if ((msg_to_server = msgget(MKEY_SERVER, 
        0666 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr("msgget");

    if (pthread_mutex_init(&protect, 0) != 0)
        syserr ("mutex init");
    if (pthread_attr_init(&attr) != 0)
        syserr ("attr init");
    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE) != 0)
        syserr ("setdetach");
    for (i = 0; i < MAX_K; ++i) {
        if (pthread_cond_init(&res_cond[i], 0) != 0)
            syserr("cond init");
        if (pthread_cond_init(&unpaired_cond[i], 0) != 0)
            syserr("cond init");
    }

    K = atoi(argv[1]);
    N = atoi(argv[2]);

    if (debug) fprintf(stderr, "Wczytano K: %d, N: %d.\n", K, N);
    
    /* inicjalizacja tablicy zasobów */
    for (i = 0; i < K; ++i)
        res[i] = N;
/*
    for ( ; i < MAX_K; ++i)
        res[i] = 0;
*/
    if (debug) res_print();
    
    i = 0;
    for ( ; ; ) {
        /* dla każdego klienta tworzony jest nowy wątek
         * który się nim zajmuje */
        if (msgrcv(msg_to_server, &msgbuf, MSG_SIZE, ALLOCATE, 0) <= 0)
            syserr("msgrcv");
        
        sscanf(msgbuf.msg_data, "%d %d %d", &pid, &k, &n);

        if (debug) fprintf(stderr, "Klient %d o pid %d rząda %d zasobu"
                            " %d\n", i, pid, n, k); 
       
        args = malloc(sizeof(*args));
        args->pid = pid;
        args->k = k;
        args->n = n;

        if (pthread_create(&th, &attr, client, (void *) args) != 0)
            syserr("pthread_create");
        
        ++i;
    }

    exit(EXIT_FAILURE);
}
