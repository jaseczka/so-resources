/********************************
 * Autor: Magdalena Teperowska
 * Plik: klient.c
 * Data: 23.12.2013
 *
 ********************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "msgkey.h"
#include "err.h"

#ifdef DEBUG
int debug = 1;
#else
int debug = 0;
#endif

int main(int argc, char* argv[])
{
    msgbuf_t msgbuf;
    int n, k, s;
    int msg_to_server, msg_to_client;
    pid_t pid;

    if ((msg_to_server = msgget(MKEY_SERVER, 0)) == -1)
        syserr("msgget");

    if ((msg_to_client = msgget(MKEY_CLIENT, 0)) == -1)
        syserr("msgget");

    pid = getpid();
    k = atoi(argv[1]);
    n = atoi(argv[2]);
    s = atoi(argv[3]);

    if (debug) fprintf(stderr, "Wczytałem k = %d, n = %d, s = %d\n",
                    k, n, s);

    msgbuf.msg_type = ALLOCATE;
    sprintf(msgbuf.msg_data, "%d %s %s %s", 
            pid, argv[1], argv[2], argv[3]);

    if (debug) fprintf(stderr, "[K: %d] Wysyłam komunikat treści: %s\n", 
                    pid, msgbuf.msg_data);

    if (msgsnd(msg_to_server, &msgbuf, MSG_SIZE, 0) != 0)
        syserr("msgsnd");

    if (msgrcv(msg_to_client, &msgbuf, MSG_SIZE, pid, 0) <= 0)
        syserr("msgrcv");

    if (debug) fprintf(stderr, "Dostałem wiadomość %s.\n", 
                    msgbuf.msg_data);
    
    fprintf(stdout, "%s %s %d %s\n", argv[1], argv[2], pid, 
                        msgbuf.msg_data);

    sleep(s);

    /* powiadomienie o zwolnieniu zasobów */
    /* typ wiadomości ten sam (pid), treść dowolna */
    if (msgsnd(msg_to_server, &msgbuf, MSG_SIZE, 0) != 0)
        syserr("msgsnd");

    printf("%d KONIEC\n", pid);

    exit(EXIT_SUCCESS);
}
