/********************************
 * Autor: Magdalena Teperowska
 * Plik: msgkey.h 
 * Data: 23.12.2013
 *
 ********************************/

#ifndef MSGQ_H
#define MSGQ_H

#define MKEY_CLIENT 1313
#define MKEY_SERVER 3131
#define MSG_SIZE 14
#define ALLOCATE 1L
#define FREE 2L

typedef struct {
    long msg_type;
    char msg_data[MSG_SIZE];
} msgbuf_t;


#endif
