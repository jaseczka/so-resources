cond for_pair[K];
cond for_res[K];
pthread_t thread[K];
int demand[K] = {0, ..., 0};

void thread(int pid, int k, int n)
{
    int pid_partnera, thread_partnera, resource_sum;

    if (!empty(for_res[k]) || empty(for_pair[k]) {
        /* 1. jakaś para czeka na zasób, czekamy na swoją kolej */
        /* 2. żadna para nie czeka, ale jestem pierwszym wątkiem
         * i czekam aż przyjdzie drugi i mnie obudzi */
        ++waiting[k];
        wait(for_pair[k]);
        /* ktoś mnie obudził z for_pair[k] */
        --waiting[k];
    }

    if (empty(for_res[k]) && !empty(for_pair[k])) {
        /* jestem drugim wątkiem z pary,
         * będę robić parę z czekającym w for_pair[k] */
        thread[k] = pthread_self();
        pid[k] = pid;
        demand[k] = n;
        signal(for_pair[k]);
        pid_partnera = pid[k];
        thread_partnera = thread[k];
        resource_sum = demand[k];
        if (demand[k] > free_resource[k])
            /* nie starczy zasobów, czekam */
            wait(for_res[k]);
    } else {
        /* 1. jakaś para czeka na zasób, czekamy na swoją kolej */
        /* 2. żadna para nie czeka, ale jestem pierwszym wątkiem
         * i czekam aż przyjdzie drugi i mnie obudzi */
        ++waiting[k];
        wait(for_pair[k]);
        /* ktoś mnie obudził z for_pair[k] */
        --waiting[k];
        /* JEŚLI jestem pierwszy: */
        if (demand[k] == 0)
            GOTO LINE 13;
        /* JEŚLI jestem drugi z pary: */
        else {
            demand[k] += n;
            pid_partnera = pid[k];
            pid[k] = pid;
            thread_partnera = thread[k];
            thread[k] = pthread_self();
            resource_sum = demand[k];
            wait(for_res[k]);
        }
    }
    /* ktoś mnie obudził z for_res[k], jem zasoby :) */
    /* jeśli jestem pierwszy budzę drugiego gościa */
    if (!empty(for_res[k])) {
        free_resource[k] -= resource_sum;
        demand[k] = 0;
        signal(for_res[k]);
    /* jeśli jestem drugim gościem budzę ew gości czekających na pary */
    } else {
        if (waiting[k] >= 2) signal(for_pair[k]);
    }
    
    SendMessage(kolejka:=to_client, typ:=pid, treść:=działaj);

    GetMessage(kolejka:=to_server, typ:=pid);

    /* czekam na drugi wątek */
    /* ale żeby nie było zakleszczenia tylko jeden z pary może czekać */
    if (pid < pid_partnera) {
        pthread_join(thread_partnera);
        /* drugi się skończył więc mogę już zwolnić zasoby */
        free_resources[k] += resource_sum;
        /* TRZEBA TU POBUDZIĆ COŚ JAKOŚ */ 
    }
}
